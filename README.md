The scripts in this project initializing linux and vim on a new system.

setup_linux is optional

1. setup_dependencies to install all dependencies
2. create_links to create the symbolic links for .vimrc, .gitconfig and .tmux_config
3. setup_vim to configure vim

~/.gitconfig.user is necessary

After setup_vim change font to "Droid Sans Mono for Powerline"
